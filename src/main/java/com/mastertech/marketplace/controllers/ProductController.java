package com.mastertech.marketplace.controllers;

import java.net.URI;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mastertech.marketplace.helpers.URIBuilder;
import com.mastertech.marketplace.models.Product;
import com.mastertech.marketplace.models.User;
import com.mastertech.marketplace.repositories.ProductRepository;
import com.mastertech.marketplace.repositories.UserRepository;

@RestController
@RequestMapping("/products")
public class ProductController {
	
	@Autowired
	ProductRepository productRepository;
	
	@Autowired
	UserRepository userRepository;
	
	@GetMapping
	public Iterable<?> getAll() {
		return productRepository.findAll();
	}
	
	@GetMapping(path="/users/{id}")
	public ResponseEntity<?> getAllByOwner(@PathVariable int id) {
		Optional<User> userQuery = userRepository.findById(id);
		
		if(userQuery.isPresent()) {
			User user = userQuery.get();
			Iterable<Product> products = productRepository.findAllByOwner(user);
			
			return ResponseEntity.ok(products);
		}
		
		return ResponseEntity.notFound().build();
	}
	
	@PostMapping
	public ResponseEntity<?> create(@RequestBody Product product) {
		Product savedProduct = productRepository.save(product);
		
		URI uri = URIBuilder.fromString("/user/" + savedProduct.getId());
		
		return ResponseEntity.created(uri).build();
	}
	
	@PutMapping(path="/{id}")
	public ResponseEntity<?> update(@PathVariable int id, @RequestBody Product updatedProduct) {
		Optional<Product> productQuery = productRepository.findById(id);
		
		if(productQuery.isPresent()) {
			Product product = productQuery.get();
			
			product.setPrice(updatedProduct.getPrice());
			product.setDescription(updatedProduct.getDescription());
			product.setTitle(updatedProduct.getTitle());
			
			productRepository.save(product);
			
			return ResponseEntity.ok(product);
		}
		
		return ResponseEntity.notFound().build();
	}
	
	@DeleteMapping(path="/{id}")
	public ResponseEntity<?> delete(@PathVariable int id) {
		Optional<Product> productQuery = productRepository.findById(id);
		
		if(productQuery.isPresent()) {
			productRepository.delete(productQuery.get());
			return ResponseEntity.ok().build();
		}
		
		return ResponseEntity.notFound().build();
	}
}	