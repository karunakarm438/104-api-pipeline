package com.mastertech.marketplace.controllers;

import java.net.URI;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mastertech.marketplace.helpers.URIBuilder;
import com.mastertech.marketplace.models.User;
import com.mastertech.marketplace.repositories.UserRepository;

@RestController
@RequestMapping("/users")
public class UserController {

	@Autowired
	UserRepository userRepository;
	
	@GetMapping
	public Iterable<User> getAll() {
		return userRepository.findAll();
	}
	
	@GetMapping(path="/{id}")
	public ResponseEntity<?> get(@PathVariable int id) {
		Optional<User> userQuery = userRepository.findById(id);
		
		if(userQuery.isPresent()) {
			User user = userQuery.get();
			return ResponseEntity.ok(user);
		}
		
		return ResponseEntity.notFound().build();
	}
	
	@PostMapping
	public ResponseEntity<?> create(@RequestBody User user) {
		User savedUser = userRepository.save(user);
		
		URI uri = URIBuilder.fromString("/user/" + savedUser.getId());
		
		return ResponseEntity.created(uri).build();
	}
	
	@PutMapping(path="/{id}")
	public ResponseEntity<?> update(@PathVariable int id, @RequestBody User updatedUser) {
		Optional<User> userQuery = userRepository.findById(id);
		
		if(userQuery.isPresent()) {
			User user = userQuery.get();
			
			user.setEmail(updatedUser.getEmail());
			user.setName(updatedUser.getName());
			
			userRepository.save(user);
			
			return ResponseEntity.ok(user);
		}
		
		return ResponseEntity.notFound().build();
	}
	
	@DeleteMapping(path="/{id}")
	public ResponseEntity<?> delete(@PathVariable int id) {
		Optional<User> userQuery = userRepository.findById(id);
		
		if(userQuery.isPresent()) {
			userRepository.delete(userQuery.get());
			return ResponseEntity.ok().build();
		}
		
		return ResponseEntity.notFound().build();
	}
}	